# WpaiaASFA

オレアアスファ（WpaiaASFA）というサイト。
のレポジトリです。

## ローカル開発環境

* ローカル開発にはMAMPを使用してください。
  MAMPのルートは「htdocs」フォルダです。
* 本プロジェクトはsassを使用しています。
  htdocs > assets > style > scss　のファイルを編集して、
  htdocs > assets > style > css　のフォルダにコンパイル結果を出力してください。

## テストサイト

URL : http://wpaiaasfa.e-bird.work/
User : asfa
Pass : qazxc

## git

.gitignore　を編集し、不要なファイルはgitアップしないようにしました。

無視するファイルリスト
ー　Thumbs.db
ー　.Ds_Store
ー　.sass-cache/　（sassのキャッシュ）