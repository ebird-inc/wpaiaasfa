<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="content-script-type" content="text/javascript" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="telephone=no" />
  <script>
    var ua = navigator.userAgent; // ユーザーエージェントの取得
    if ((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)) {
        // スマホのとき
        document.write('<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">');
    }else{
        // PC・タブレットのとき
        document.write('<meta name="viewport" content="width=1440, maximum-scale=1.0, user-scalable=yes">');
    }
  </script>
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/assets/images/favicon.ico" />
  <link rel="icon" type="image/x-icon" href="/assets/images/favicon.ico" />

  <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png" />
  <link rel="canonical" href="https://asfa-uenomedic.com/" />

  <meta name=”description” content=”オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。次亜塩素酸ナトリウムと希塩酸を純水で希釈混合し中性領域（6.8〜7.0）にpH調整しています。濃度は100ppm。財団法人による一部ウイルス有効性試験検査済みです。”/>
  <meta property="og:title" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://asfa-uenomedic.com/" />
  <meta property="og:image" content="https://asfa-uenomedic.com/assets/images/ogimage.png" />
  <meta property="og:site_name" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。" />
  <meta property="og:description" content="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤。オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。" />
  <!-- ※Twitter共通設定 -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。">
  <meta name="twitter:description" content="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤。オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。">
  <meta name="twitter:image:src" content="https://asfa-uenomedic.com/assets/images/ogimage.png">

  <title>次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。</title>

  <link rel="stylesheet" type="text/css" href="/assets/style/css/style.css" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180278838-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-180278838-1');
  </script>

</head>
<body>
  <header>
    <div class="logo">
      <img class="logoimg" src="/assets/images/logo@2x.png" alt="オレアアスファ">
      <button id="logotoggle" class="toggleBtn">
      </button>  
    </div>
    <div id="menutoggle" class="menu">
      <ul>
        <li><a href="#about">オレアアスファとは？</a></li>
        <li><a href="#product">製品仕様</a></li>
        <li><a href="#contact">お問い合わせ</a></li>
        <li><a href="#buy">購入</a></li>
        <li class="sns">
          <a
            href="https://twitter.com/intent/tweet?text=厚労省、国立感染症研究所が公式に推奨する成分の除菌剤&url=https://asfa-uenomedic.com/"
            target="_blank"
            rel="nofollow noopener noreferrer"
          >
            <img src="/assets/images/twitter_logo.png" alt="twitter_logo">
          </a>
          <a
            href="https://www.facebook.com/sharer/sharer.php?u=https://asfa-uenomedic.com/"
            target="_blank"
            rel="nofollow noopener noreferrer"
          >
            <img src="/assets/images/facebook_logo.png" alt="facebook_logo">
          </a>
        </li>
      </ul>
    </div>
  </header>

  <div class="heading">
    <div class="whole">
      <div class="container">
        <div class="textbox">
          <div class="text">
            <h1 class="sub">
              厚労省、国立感染症研究所が<br>
              公式に推奨する成分の除菌剤
            </h1>
            <p class="main">
              オレアアスファ
            </p>
            <p class="option">
              2リットルポリタンク入り
            </p>
            <p class="option opensell">
              オープンキャンペーンにつき<br>
              <span>※キャンペーン期間　<br class="sp-only">2020年10月19日～2020年11月18日</span>
            </p>
            <p class="price">
              4,000 <span class="unit">円</span><span class="tax">/本（税込）</span>
            </p>
            <p class="discount">
              <span class="num">4,500</span> <span class="unit">円</span><span class="tax">（税込）</span>
            </p>
            <br>
            <div class="further">
              <span>さらに今なら</span>
              <p>
                <span>2本以上の購入で、</span><span class="num">3,800</span><span class="unit">円</span><span class="tax">/本（税込）</span>
              </p>
              <p>
                <span>4本以上の購入で、</span><span class="num">3,650</span><span class="unit">円</span><span class="tax">/本（税込）</span>
              </p>
              <p>
                <span>6本以上の購入で、</span><span class="num">3,500</span><span class="unit">円</span><span class="tax">/本（税込）</span>
              </p>
              <p class="option opensell">
                まとめて６本購入なら3,000円お得！
              </p>
            </div>
          </div>
        </div>
        <div class="image">
          <img src="/assets/images/headingicon@2x.png" alt="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤">
        </div>
      </div>

      <div class="storesbtn">
        <div class="storepkg">
          <div class="storesjp-button" data-storesjp-item="5f02e3dc74b4e45ed59a616a" data-storesjp-variation="5f02e3dc74b4e45ed59a616b" data-storesjp-name="unicorn022" data-storesjp-layout="layout_d" data-storesjp-lang="ja" ></div><script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
          <p class="storetxt">
            <span>▲ 3本未満の方はこちら</span>
          </p>
        </div>
        <div class="storepkg">
          <div class="storesjp-button" data-storesjp-item="5f8021103313d27500bd8864" data-storesjp-variation="5f8021103313d27500bd8866" data-storesjp-name="unicorn022" data-storesjp-layout="layout_d" data-storesjp-lang="ja" ></div><script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
          <p class="storetxt">
            <span>▲ 4本~5本購入の方はこちら</span>
          </p>
        </div>
        <div class="storepkg">
          <div class="storesjp-button" data-storesjp-item="5f8021548ac3942ce7a2f294" data-storesjp-variation="5f8021548ac3942ce7a2f296" data-storesjp-name="unicorn022" data-storesjp-layout="layout_d" data-storesjp-lang="ja" ></div><script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
          <p class="storetxt">
            <span>▲ 6本以上購入の方はこちら</span>
          </p>
        </div>
      </div>
    </div>

  </div>

  <div class="column1">
    <div class="mainbox">
      <p class="title">今お使いの除菌剤、<br class="sp-only">中身は安全ですか？</p>
      <p class="para">
        現在、市場には高機能や安全性を唄う数多くの除菌剤が流通しております。<br class="pc-only">
        中には、次亜塩素酸のラベルがありながら中身が偽物であったり、希釈された物などが当たり前のように流通しています。<br class="pc-only">
        「オレアアスファ」は、厚労省や国立感染症研究所公式プレスでもその効果について言及され、<br class="pc-only">
        <span class="strong">財団法人日本食品分析センターでの検査を経た除菌剤です。</span><br class="pc-only">
        <span class="strong">さらに月島ほけん整骨院では、生産物賠償責任保険にも加入。</span><br class="pc-only">
        第三者機関による品質保証に注力した製品をお届けしております。<br class="pc-only">
        家族、社員、会社、お店、お客様、あなたにとって大切なモノを守るため。<br class="pc-only">
        公的な証明や第三者機関のチェックがある製品をお届けいたします。
      </p>
    </div>
    <div class="cautionContainer">
      <div class="cautionbox">
        <img class="mark" src="/assets/images/cautionmark.svg" alt="当社取り扱い品限定で品質保証を致します。">
        <p class="para">
          当社取り扱い品限定で品質保証を致します。<br class="pc-only">
          除菌剤の各種正規品の箱に品質保証のされていない偽物商品を入れて売りに出されている例が多発しています。ご注意下さい。
          当社取り扱い商品は全て第三者機関による品質保証がされ、生産物賠償責任保険の対象とされていますのでご安心下さい。<br class="pc-only">
          (ラベルだけ貼り替えた偽物や、保存方法等の正しい品質管理ができていない商品にご注意ください。<br class="pc-only">
          そのため、同名商品であっても他社プロデュース品の品質は保証しません。）<br class="pc-only">
          当社オレアアスファプラスは、器具用です。室内、食品、テーブル、トイレ、キッチン等の除菌・消臭に有用です。
        </p>
      </div>
    </div>
  </div>

  <div id="about" class="column2">
    <div class="mainbox">
      <p class="title">オレアアスファとは？</p>
      <p class="sub">ABOUT PRODUCT</p>
      <p class="para">
        オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。<br class="pc-only">
        食品添加物仕様の次亜塩素酸ナトリウム（アルカリ性）と希塩酸（酸性）を水で<span class="strong">希釈混合</span>し、ほぼ<span class="strong">中性領域（6.8～7.0）にpH調整</span>しています。<br class="pc-only">
        従来の殺菌・除菌剤である次亜塩素酸ナトリウムは強い分解力を持つ反面、手荒れ・漂白など人体・環境に影響を及ぼす恐れがあります。<br class="pc-only">
        オレアアスファはほぼ中性領域にpH調整することで、<span class="strong">強い分解力を持ちながら人体・環境にも優しい安全な除菌・消臭水</span>です。
      </p>
    </div>
    <div class="about img1">
      <img class="aboutimg" src="/assets/images/about1@2x.png" alt="次亜塩素酸ナトリウムを原料とした原液のままで安全に使用できる除菌剤です。">
      <div class="textbox">
        <div class="text">
          <p class="main">
            次亜塩素酸ナトリウムを原料とした、
            原液のままで安全に使用できる
            除菌剤です。
          </p>
          <p class="para">
            原料は、厚⽣労働省が⾷品添加物としての使⽤を許可している次亜塩
            素酸ナトリウム（NaCIO）と希塩酸（HCI）、と純⽔、のみです。
            ⾷品⼯場等では、⾷品衛⽣管理基準（HACCP)をクリアするために活
            ⽤されています。
          </p>
        </div>
      </div>
    </div>
    <div class="about img2">
      <img class="aboutimg" src="/assets/images/about2@2x.png" alt="厚⽣労働省や国⽴感染症研究所が医療機関・検査機関へ新型コロナウイルスの除菌に推奨しているものと同⼀成分を使⽤">
      <div class="textbox">
        <div class="text">
          <p class="main">
            厚⽣労働省や国⽴感染症研究所が
            医療機関・検査機関へ
            新型コロナウイルスの除菌に
            推奨しているものと同⼀成分を使⽤
          </p>
          <p class="para margin">
            厚⽣労働省のウェブサイトより<br class="pc-only">
            「物の表⾯の消毒には次亜塩素酸ナトリウム（0.1%）が有効であるこ<br class="pc-only">
            とが分かっています」
          </p>
          <p class="para">
            NIID 国⽴感染症研究所のウェブサイトより<br class="pc-only">
            「医療機関においては、患者周囲の⾼頻度接触部位などはアルコールあ<br class="pc-only">
            るいは0.05%の次亜塩素酸ナトリウムによる清拭で⾼頻度接触⾯や物<br class="pc-only">
            品等の消毒の励⾏が望ましい」
          </p>
        </div>
      </div>
    </div>
    <div class="about img3">
      <img class="aboutimg" src="/assets/images/about3@2x.png" alt="「オレアアスファ」の安全性は高く、誤って口に入れても人体に影響がありません。">
      <div class="textbox">
        <div class="text">
          <p class="main">
            「オレアアスファ」の安全性は高く、
            誤って口に入れても
            人体に影響がありません。
          </p>
          <p class="para margin">
            有機物に触れると⽔に還元され、塩素が残留しません。キッチンなど⾷
            材を扱う場所、あかちゃんのいる場所でも使⽤できます。万が⼀誤って
            ⼝に⼊れても⼈体に影響がありません。
          </p>
          <a id="btntoresultdoc" class="btn" href="#resultdoc">
            <p class="btnpara">
              赤ちゃんの目や口に入れても<br>
              安心であるエビデンス及び研究実験資料はこちら
            </p>
          </a>
        </div>
      </div>
    </div>

    <div class="other">
      <a class="bigbtn" href="news.php">
        <p class="bigbtnpara">
        ウィルス有効性試験、安全性試験、医学的エビデンス、研究実験資料等の公的プレスや<br class="pc-only">ニュースなどで取り上げられた記事をこちらにまとめております。
        </p>
      </a>
    </div>
  </div>

  <div id="product" class="column3">
    <div class="mainbox">
      <p class="title">製品仕様</p>
      <p class="sub">ABOUT PRODUCT</p>
      <p class="para">
        オレアアスファ<br class="sp-only">(微酸性次亜塩素酸水）
      </p>
      <p class="caution">※原液のままご使用ください。</p>
    </div>
    <div class="specbox">
      <dl>
        <dt>製造方法</dt>
        <dd>混合方式</dd>
      </dl>
      <dl>
        <dt>原料</dt>
        <dd>
          次亜塩素酸ナトリウム （NaClO）　<span class="smallfont">※食品添加物仕様</span><br>
          希塩酸 （HCl）　<span class="smallfont">※食品添加物仕様</span><br>
          純水
        </dd>
      </dl>
      <dl>
        <dt>pH</dt>
        <dd>6.8～7.0 （中性領域）</dd>
      </dl>
      <dl>
        <dt>濃度</dt>
        <dd>100ppm</dd>
      </dl>
      <dl>
        <dt>用途</dt>
        <dd>除菌・消臭</dd>
      </dl>
      <dl>
        <dt>分類</dt>
        <dd>一般雑貨</dd>
      </dl>
      <dl>
        <dt>使用期限</dt>
        <dd>6ヶ月を目安にご使用ください。</dd>
      </dl>
      <dl>
        <dt>保管方法</dt>
        <dd>熱と光にとても弱いため、日の当たらない冷暗所にて保管ください。</dd>
      </dl>
    </div>
    <div class="listbox">
      <ul>
        <li>財団法人による一部ウィルス有効性試験検査済み</li>
        <li>製造者責任保険加入済み</li>
        <li>ハラール認証取得済</li>
      </ul>
    </div>
  </div>
  
  <div id="buy" class="column4">
    <div class="mainbox">
      <p class="title">購入</p>
      <p class="sub">PURCHASE</p>
    </div>
    <div class="maincontainer">
      <div class="container">
        <div class="textbox">
          <div class="image">
            <img src="/assets/images/producticon@2x.png" alt="オレアアスファ">
          </div>
          <div class="text">
            <div class="box">
              <p class="main">
                オレアアスファ
              </p>
              <p class="option">
                2リットルポリタンク入り
              </p>
              <p class="option opensell">
                オープンキャンペーンにつき<br>
                <span>※キャンペーン期間　<br class="sp-only">2020年10月19日～2020年11月18日</span>
              </p>
              <p class="price">
                4,000 <span class="unit">円</span><span class="tax">/本（税込）</span>
              </p>
              <p class="discount">
                <span class="num">4,500</span> <span class="unit">円</span><span class="tax">（税込）</span>
              </p>
              <div class="further">
                <span>さらに今なら</span>
                <p>
                  <span>2本以上の購入で、</span><span class="num">3,800</span><span class="unit">円</span><span class="tax">/本（税込）</span>
                </p>
                <p>
                  <span>4本以上の購入で、</span><span class="num">3,650</span><span class="unit">円</span><span class="tax">/本（税込）</span>
                </p>
                <p>
                  <span>6本以上の購入で、</span><span class="num">3,500</span><span class="unit">円</span><span class="tax">/本（税込）</span>
                </p>
                <p class="option opensell">
                  まとめて６本購入なら3,000円お得！
                </p>
              </div>
            </div>
          </div>
        </div>

        <div class="storesbtn">
          <div class="storepkg">
            <div class="storesjp-button" data-storesjp-item="5f02e3dc74b4e45ed59a616a" data-storesjp-variation="5f02e3dc74b4e45ed59a616b" data-storesjp-name="unicorn022" data-storesjp-layout="layout_d" data-storesjp-lang="ja" ></div><script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
            <p class="storetxt">
              <span>▲ 3本未満の方はこちら</span>
            </p>
          </div>
          <div class="storepkg">
            <div class="storesjp-button" data-storesjp-item="5f8021103313d27500bd8864" data-storesjp-variation="5f8021103313d27500bd8866" data-storesjp-name="unicorn022" data-storesjp-layout="layout_d" data-storesjp-lang="ja" ></div><script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
            <p class="storetxt">
              <span>▲ 4本~5本購入の方はこちら</span>
            </p>
          </div>
          <div class="storepkg">
            <div class="storesjp-button" data-storesjp-item="5f8021548ac3942ce7a2f294" data-storesjp-variation="5f8021548ac3942ce7a2f296" data-storesjp-name="unicorn022" data-storesjp-layout="layout_d" data-storesjp-lang="ja" ></div><script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
            <p class="storetxt">
              <span>▲ 6本以上購入の方はこちら</span>
            </p>
          </div>
        </div>

        <div class="caution spray">
          <div class="cautionbox spray">
            <img class="mark spray" src="/assets/images/spray.svg" alt="容器を移し替えることで、身近なところに常備できます。小さな容器なら持ち運びも簡単です。">
            <p class="para spray">
              容器を移し替えることで、<br class="sp-only">身近なところに常備できます。<br>
              小さな容器なら持ち運びも簡単です。
            </p>
          </div>
        </div>
        <br>
        <div class="caution">
          <div class="cautionbox">
            <img class="mark" src="/assets/images/cautionmark.svg" alt="当社取り扱い品限定で品質保証を致します。">
            <p class="para">
              当社取り扱い品限定で品質保証を致します。<br class="pc-only">
              除菌剤の各種正規品の箱に品質保証のされていない偽物商品を入れて売りに出されている例が多発しています。ご注意下さい。
              当社取り扱い商品は全て第三者機関による品質保証がされ、生産物賠償責任保険の対象とされていますのでご安心下さい。<br class="pc-only">
              (ラベルだけ貼り替えた偽物や、保存方法等の正しい品質管理ができていない商品にご注意ください。<br class="pc-only">
              そのため、同名商品であっても他社プロデュース品の品質は保証しません。）<br class="pc-only">
              当社オレアアスファプラスは、器具用です。室内、食品、テーブル、トイレ、キッチン等の除菌・消臭に有用です。
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="contact" class="contact">
    <div class="mainbox">
      <p class="title">お問い合わせ</p>
      <p class="sub">CONTACT</p>
      <p class="para">商品へのお問合せは以下のメールアドレスにお気軽にご連絡ください。</p>
      <a class="mailbtn" href="mailto:asfa@asfa-uenomedic.com">asfa@asfa-uenomedic.com</a><br>
      <a href="/trade/" class="notationbox">
        <img class="notationImg" src="/assets/images/arrow4.svg" alt="特定商取引法に関する表記">
        <span class="notation">特定商取引法に関する表記</span>
      </a>
  </div>
  </div>

  <footer>
    <div class="mainbox">
      <p class="company">月島ほけん整骨院</p>
      <p class="address">東京都中央区月島2-10-1</p>
      <p>03－5560－0120</p>
    </div>
    <div class="copyright">
      <small>Copyright(C)  wpaia co.Ltd. All Rights Reserved.</small>
    </div>
  </footer>

  <script src="/assets/js/jquery-3.5.1.min.js"></script>
  <script src="/assets/js/common.js"></script>
</body>
</html>