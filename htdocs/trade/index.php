<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="content-script-type" content="text/javascript" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="telephone=no" />
  <script>
    var ua = navigator.userAgent; // ユーザーエージェントの取得
    if ((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)) {
        // スマホのとき
        document.write('<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">');
    }else{
        // PC・タブレットのとき
        document.write('<meta name="viewport" content="width=1440, maximum-scale=1.0, user-scalable=yes">');
    }
  </script>
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/assets/images/favicon.ico" />
  <link rel="icon" type="image/x-icon" href="/assets/images/favicon.ico" />

  <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png" />
  <link rel="canonical" href="https://asfa-uenomedic.com/" />

  <meta name=”description” content=”オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。次亜塩素酸ナトリウムと希塩酸を純水で希釈混合し中性領域（6.8〜7.0）にpH調整しています。濃度は100ppm。財団法人による一部ウイルス有効性試験検査済みです。”/>
  <meta property="og:title" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://asfa-uenomedic.com/" />
  <meta property="og:image" content="https://asfa-uenomedic.com/assets/images/ogimage.png" />
  <meta property="og:site_name" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。" />
  <meta property="og:description" content="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤。オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。" />
  <!-- ※Twitter共通設定 -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。">
  <meta name="twitter:description" content="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤。オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。">
  <meta name="twitter:image:src" content="https://asfa-uenomedic.com/assets/images/ogimage.png">

  <title>次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。</title>

  <link rel="stylesheet" type="text/css" href="/assets/style/css/style.css" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180278838-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-180278838-1');
  </script>

</head>
<body>
  <header>
    <div class="logo">
      <img class="logoimg" src="/assets/images/logo@2x.png" alt="オレアアスファ">
      <button id="logotoggle" class="toggleBtn">
      </button>  
    </div>
    <div id="menutoggle" class="menu">
      <ul>
        <li><a href="/#about">オレアアスファとは？</a></li>
        <li><a href="/#product">製品仕様</a></li>
        <li><a href="/#contact">お問い合わせ</a></li>
        <li><a href="/#buy">購入</a></li>
        <li class="sns">
          <a
            href="https://twitter.com/intent/tweet?text=厚労省、国立感染症研究所が公式に推奨する成分の除菌剤&url=https://asfa-uenomedic.com/trade/"
            target="_blank"
            rel="nofollow noopener noreferrer"
          >
            <img src="/assets/images/twitter_logo.png" alt="twitter_logo">
          </a>
          <a
            href="https://www.facebook.com/sharer/sharer.php?u=https://asfa-uenomedic.com/trade/"
            target="_blank"
            rel="nofollow noopener noreferrer"
          >
            <img src="/assets/images/facebook_logo.png" alt="facebook_logo">
          </a>
        </li>
      </ul>
    </div>
  </header>

  <div class="trade_column1">
    <div class="mainbox">
      <h2 class="title">特定商取引法に関する表記</h2>
      <h3 class="label">販売者</h3>
      <p class="desc">月島ほけん整骨院</p>
      <h3 class="label">所在地</h3>
      <p class="desc">東京都中央区月島2－10－1－B102</p>
      <h3 class="label">連絡先</h3>
      <p class="desc">03－5560－0120</p>
      <h3 class="label">代表者</h3>
      <p class="desc">伊香徹也</p>
      <h3 class="label">商品代金以外に必要な料金</h3>
      <p class="desc">無し</p>
      <h3 class="label">お支払い方法</h3>
      <p class="desc line">
        クレジットカード<br>
        銀行振込（先払い）<br>
        三菱UFJ銀行　深川支店　普通　1911604<br>
        名義　伊香徹也（イコウテツヤ）
      </p>
      <h3 class="label">支払い時期</h3>
      <p class="desc">前払い</p>
      <h3 class="label">商品のお引き渡し時期</h3>
      <p class="desc">着金確認後、即時配送手続き</p>
      <h3 class="label">キャンセル、返品</h3>
      <p class="desc">不可</p>
      <h3 class="label">◆ 支払い方法について</h3>
      <p class="desc line">
        クレジットカード・銀行振り込みのみ<br>
        注文後の支払い方法変更はできかねます。<br>
        もし支払い方法変更希望の場合は一度受注をキャンセルする必要があるため、お問い合わせフォームよりご連絡をお願いします。
      </p>
      <h3 class="label">◆ 返品について</h3>
      <p class="desc line">
        商品に欠陥がある場合を除き、返品には応じません（未使用の場合はこの限りではありません）<br>
        商品に欠陥があった際は商品到着後、10日以内にお問い合わせフォームよりご連絡をお願いします。<br>
        欠陥がある商品は着払いにてご返送いただき、直ちに良品を再配送させていただきます。
      </p>
      <h3 class="label">◆ 商品の配送方法及び引き渡し時期</h3>
      <p class="desc line">
        佐川急便様の宅急便でお届けいたします。<br>
        配送のご依頼を受けてから7日以内に発送いたします。
      </p>
      <h3 class="label">◆ 転売に対する対応について</h3>
      <p class="desc line">
        上野メディカル支援センター合同会社の公式サイト以外で販売されている商品につきましては品質並びに安全性は保証致しかねますのでご注意ください。<br>
        転売した商品によりご購入者や当社が不利益・被害を受けた場合、販売元へ賠償請求致します。
      </p>
    </div>
  </div>

  <div id="contact" class="contact">
    <div class="mainbox">
      <p class="title">お問い合わせ</p>
      <p class="sub">CONTACT</p>
      <p class="para">商品へのお問合せは以下のメールアドレスにお気軽にご連絡ください。</p>
      <a class="mailbtn" href="mailto:asfa@asfa-uenomedic.com">asfa@asfa-uenomedic.com</a><br>
      <a href="/trade/" class="notationbox">
        <img class="notationImg" src="/assets/images/arrow4.svg" alt="特定商取引法に関する表記">
        <span class="notation">特定商取引法に関する表記</span>
      </a>
  </div>
  </div>

  <footer>
    <div class="mainbox">
      <p class="company">月島ほけん整骨院</p>
      <p class="address">東京都中央区月島2-10-1</p>
      <p>03－5560－0120</p>
    </div>
    <div class="copyright">
      <small>Copyright(C)  wpaia co.Ltd. All Rights Reserved.</small>
    </div>
  </footer>

  <script src="/assets/js/jquery-3.5.1.min.js"></script>
  <script src="/assets/js/common.js"></script>
</body>
</html>