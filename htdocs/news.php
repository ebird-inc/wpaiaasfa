<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="content-script-type" content="text/javascript" />
  <meta http-equiv="content-style-type" content="text/css" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="telephone=no" />
  <script>
    var ua = navigator.userAgent; // ユーザーエージェントの取得
    if ((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)) {
        // スマホのとき
        document.write('<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">');
    }else{
        // PC・タブレットのとき
        document.write('<meta name="viewport" content="width=1440, maximum-scale=1.0, user-scalable=yes">');
    }
  </script>
  <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/assets/images/favicon.ico" />
  <link rel="icon" type="image/x-icon" href="/assets/images/favicon.ico" />

  <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png" />
  <link rel="canonical" href="https://asfa-uenomedic.com/" />

  <meta name=”description” content=”オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。次亜塩素酸ナトリウムと希塩酸を純水で希釈混合し中性領域（6.8〜7.0）にpH調整しています。濃度は100ppm。財団法人による一部ウイルス有効性試験検査済みです。”/>
  <meta property="og:title" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://asfa-uenomedic.com/" />
  <meta property="og:image" content="https://asfa-uenomedic.com/assets/images/ogimage.png" />
  <meta property="og:site_name" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。" />
  <meta property="og:description" content="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤。オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。" />
  <!-- ※Twitter共通設定 -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。">
  <meta name="twitter:description" content="厚労省、国立感染症研究所が公式に推奨する成分の除菌剤。オレアアスファは幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液です。">
  <meta name="twitter:image:src" content="https://asfa-uenomedic.com/assets/images/ogimage.png">

  <title>次亜塩素酸水溶液オレアアスファ。pH6.8〜7.0、100ppm。</title>

  <link rel="stylesheet" type="text/css" href="/assets/style/css/style.css" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180278838-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-180278838-1');
  </script>

</head>
<body>

  <div class="news_column1">
    <div class="mainbox">
      <p class="title">
        ウィルス有効性試験、安全性試験、医学的エビデンス、<br class="pc-only">研究実験資料等の公的プレス
      </p>
      <p class="para">
        幅広い場面で除菌・消臭に使用できる次亜塩素酸水溶液の感染症に対する除菌効果について<br class="pc-only">
        最新の情報をまとめてまいります。
      </p>
    </div>

    <div class="maincontainer">

      <ul class="newslist">
        <li>
          <p class="title">新型コロナの消毒法、次亜塩素酸水も有効…経産省ら最終報告</p>
          <div class="detail">
            <p class="detail_title">Yahoo!ニュース</p>
            <a class="detail_link" target=”_blank” href="https://headlines.yahoo.co.jp/hl?a=20200629-00000004-resemom-life.view-000">https://headlines.yahoo.co.jp/hl?a=20200629-00000004-resemom-life.view-000</a>
          </div>
        </li>
        <li>
          <p class="title">新型コロナウイルスに対する消毒方法の有効性評価について最終報告をとりまとめました。<br class="pc-only">～物品への消毒に活用できます～</p>
          <div class="detail">
            <p class="detail_title">nite 独立行政法人　製品評価技術基盤機構</p>
            <a class="detail_link" target=”_blank” href="https://www.nite.go.jp/information/osirase20200626.html">https://www.nite.go.jp/information/osirase20200626.html</a>
          </div>
        </li>
        <li>
          <p class="title">次亜塩素酸水で新型コロナ不活化「30秒以下で」　北海道大学とエナジック社が実証</p>
          <div class="detail">
            <p class="detail_title">沖縄タイムス</p>
            <a class="detail_link" target=”_blank” href="https://www.okinawatimes.co.jp/articles/-/571040">https://www.okinawatimes.co.jp/articles/-/571040</a>
          </div>
        </li>
        <li>
          <p class="title">感染の疑いがある患者を診察する際、医療者はどのような準備や装備が必要ですか？</p>
          <div class="detail">
            <p class="detail_title">厚生労働省</p>
            <a class="detail_link" target=”_blank” href="https://www.mhlw.go.jp/stf/seisakunitsuite/bunya/kenkou_iryou/dengue_fever_qa_00004.html#Q8">https://www.mhlw.go.jp/stf/seisakunitsuite/bunya/kenkou_iryou/dengue_fever_qa_00004.html#Q8</a>
          </div>
        </li>
        <li>
          <p class="title">新型コロナウイルス感染症に対する感染管理</p>
          <div class="detail">
            <p class="detail_title">NIID　国立感染症研究所</p>
            <a class="detail_link" target=”_blank” href="https://www.niid.go.jp/niid/ja/diseases/ka/corona-virus/2019-ncov/2484-idsc/9310-2019-ncov-01.html">https://www.niid.go.jp/niid/ja/diseases/ka/corona-virus/2019-ncov/2484-idsc/9310-2019-ncov-01.html</a>
          </div>
        </li>
        <li>
          <p class="title">「次亜塩素酸水」の普及目指す団体に、噴霧反対の医師や科学者が苦言　「効くならば…」</p>
          <div class="detail">
            <p class="detail_title">BuzzFeed.News</p>
            <a class="detail_link" target=”_blank” href="https://www.buzzfeed.com/jp/kensukeseya/covid-mist-2">https://www.buzzfeed.com/jp/kensukeseya/covid-mist-2</a>
          </div>
        </li>
        <li>
          <p class="title">一般消毒剤とオレアアスファの比較</p>
          <img class="image" src="/assets/images/news/newlistimg1@2x.png" alt="一般消毒剤とオレアアスファの比較">
          <div class="detail">
            <p class="detail_title">株式会社オレア</p>
            <a class="detail_link" target=”_blank” href="http://wpaia.co.jp/shohin-jokin.html">http://wpaia.co.jp/shohin-jokin.html</a>
          </div>
        </li>
        <li>
          <p class="title">感染の主たる原因となる菌に対する分解力</p>
          <img class="image" src="/assets/images/news/newlistimg2@2x.png" alt="感染の主たる原因となる菌に対する分解力">
          <div class="detail">
            <p class="detail_title">株式会社オレア</p>
            <a class="detail_link" target=”_blank” href="http://wpaia.co.jp/shohin-jokin.html">http://wpaia.co.jp/shohin-jokin.html</a>
          </div>
        </li>
        <li>
          <p class="title">インフルエンザ不活性化試験</p>
          <ul class="block">
            <li><img src="/assets/images/news/result3-1@2x.png" alt="インフルエンザ不活性化試験1"></li>
            <li><img src="/assets/images/news/result3-2@2x.png" alt="インフルエンザ不活性化試験2"></li>
            <li><img src="/assets/images/news/result3-3@2x.png" alt="インフルエンザ不活性化試験3"></li>
            <li><img src="/assets/images/news/result3-4@2x.png" alt="インフルエンザ不活性化試験4"></li>
          </ul>
          <div class="detail">
            <p class="detail_title">株式会社オレア</p>
            <a class="detail_link" target=”_blank” href="http://wpaia.co.jp/shohin-jokin.html">http://wpaia.co.jp/shohin-jokin.html</a>
          </div>
        </li>
        <li>
          <p class="title">オレアアスファ水・エビデンス及び研究実験資料</p>
          <img class="image" src="/assets/images/news/result4@2x.png" alt="オレアアスファ水・エビデンス及び研究実験資料">
          <div class="detail">
            <p class="detail_title">株式会社オレア</p>
            <a class="detail_link" target=”_blank” href="http://wpaia.co.jp/shohin-jokin.html">http://wpaia.co.jp/shohin-jokin.html</a>
          </div>
        </li>
      </ul>
    </div>
  </div>

  <div class="news_banner">
    <a href="/">
      <img src="/assets/images/news/banner@2x.png" alt="オレオアスファの購入はこちら">
    </a>
  </div>

  <footer>
    <div class="mainbox">
      <p class="company">月島ほけん整骨院</p>
      <p class="address">東京都中央区月島2-10-1</p>
      <p>03－5560－0120</p>
    </div>
    <div class="copyright">
      <small>Copyright(C)  wpaia co.Ltd. All Rights Reserved.</small>
    </div>
  </footer>

</body>
</html>