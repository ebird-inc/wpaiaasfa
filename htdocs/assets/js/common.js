// *************** メニュー ***************
const toggleBtn = document.querySelector('#logotoggle');
const menutoggle = document.querySelector('#menutoggle');

// *************** 「赤ちゃんの目や口に...」 ***************
const btntoresultdoctoggle = document.querySelector('#btntoresultdoc');

//スマホ時のみ動作
if (window.matchMedia("(max-width: 750px)").matches) {
  
  //トップページへアンカ
  $(document).ready(function(){

    var headerHight = 65; //固定ヘッダーの高さ66
    var speed = 1000;

    var urlHash = location.hash;
    if(urlHash) {
      $('body,html').stop().scrollTop(0);
      setTimeout(function(){
        var target = $(urlHash);
        var position = target.offset().top - headerHight;
        $('body,html').stop().animate({scrollTop:position}, speed, 'swing');
      }, 500); //speed
    }

  });

  // *************** メニュー ***************
  toggleBtn.addEventListener('click', () => {
    menutoggle.classList.toggle('active');
    toggleBtn.classList.toggle('active');
  });

  //メニュークリック時
  $('#menutoggle a[href^="#"]').click(function(){

    //メニュー非表示
    menutoggle.classList.remove('active');
    toggleBtn.classList.remove('active');

    AnchorTransitionMenu();

  });
  // *************** メニュー ***************


  if(btntoresultdoctoggle != null) {
    // *************** 「赤ちゃんの目や口に...」 ***************
    btntoresultdoctoggle.addEventListener('click', AnchorTransitionSingle);
    // *************** 「赤ちゃんの目や口に...」 ***************
  }

} else {
  window.addEventListener('scroll', ()=> {
    fixMenu();
  });
}


function AnchorTransitionMenu() {
  var href = event.target.getAttribute('href');
  AnchorTransition(href);
}

function AnchorTransitionSingle() {
  var href = $(this).attr('href');
  AnchorTransition(href);
}

function AnchorTransition(arg) {
  var href = arg;

  //アンカー遷移
  var headerHight = 65; //固定ヘッダーの高さ66
  var speed = 1000;
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top - headerHight;
  $('body,html').animate({ scrollTop: position }, speed, 'swing');
}

function fixMenu() {
  let menuElement = document.getElementById('menutoggle');
  if (window.pageYOffset >= 132) {
    menuElement.classList.add('fixed');
  } else {
    menuElement.classList.remove("fixed");
  }
}